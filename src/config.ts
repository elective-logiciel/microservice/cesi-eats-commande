const assert = require("assert");
const dotenv = require("dotenv");

// read in the .env file
dotenv.config();

// capture the environment variables the application needs
const { 
    PORT,
    HOST,
    MONGO_SERVER,
    MONGO_DATABASE,
    AUTH_SERVER_ADDRESS,
    AUTH_SERVER_PORT,
    LOG_SERVER_ADDRESS,
    LOG_SERVER_PORT,
    MICROSERVICE_NAME,
} = process.env;


// validate the required configuration information
assert(PORT, "PORT configuration is required.");
assert(HOST, "HOST configuration is required.");
assert(MONGO_SERVER, "MONGO_SERVER configuration is required.");
assert(MONGO_DATABASE, "MONGO_DATABASE configuration is required.");
assert(AUTH_SERVER_ADDRESS, "AUTH_SERVER_ADDRESS configuration is required.");
assert(AUTH_SERVER_PORT, "AUTH_SERVER_PORT configuration is required.");
assert(LOG_SERVER_PORT, "LOG_SERVER_PORT configuration is required.");
assert(LOG_SERVER_ADDRESS, "LOG_SERVER_PORT configuration is required.");
assert(MICROSERVICE_NAME, "MICROSERVICE_NAME configuration is required.");

// export the configuration information
module.exports = {
    service_name: MICROSERVICE_NAME,
    port: PORT,
    host: HOST,
    mongo: {
        server: MONGO_SERVER,
        database: MONGO_DATABASE,
    },
    auth: {
        address: AUTH_SERVER_ADDRESS,
        port: AUTH_SERVER_PORT,
    },
    log: {
        address: LOG_SERVER_ADDRESS,
        port: LOG_SERVER_PORT,
    },
};