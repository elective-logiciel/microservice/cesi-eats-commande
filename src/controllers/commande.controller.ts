import { Router } from 'express';
import { Commande } from '../models/commande.model';
import { Product, ConvertHttpRequestToProduct } from '../models/product.model';
import { Article } from '../models/article.model';
import { Address } from '../models/address.model';
import { commandeService } from '../datas/commande.data';
import { returnCreated, returnSuccess, returnUpdated } from '../errors/success';
import { Error404 } from '../errors/errors';

const commandeController = Router();
const commandeSvc = new commandeService()

commandeController.get('/get/:id_commande',async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande: Commande = new Commande({
            id_commande: req.params.id_commande
        })

        commande.IsIdCommandeValid()

        const commande_res = await commandeSvc.GetCommandeById(commande)        

        const message: string = 'Get commande by id'
        returnSuccess(res, commande_res, message)

    } catch (err) {
        next(err)
    }
})

commandeController.get('/client/:id_client(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande: Commande = new Commande({
            id_client: req.params.id_client
        })
        // const id_commande: string = req.params.id_commande
        commande.IsIdClientValid()

        const commande_res = await commandeSvc.GetAllCommandesByIdClient(commande)

        const message: string = 'Get all commandes by id_client'
        returnSuccess(res, commande_res, message)

    } catch (err) {
        next(err)
    }
})

commandeController.get('/restaurateur/:id_restaurateur(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande: Commande = new Commande({
            id_restaurateur: req.params.id_restaurateur
        })
        // const id_commande: string = req.params.id_commande
        commande.IsIdRestaurateurValid()

        const commande_res = await commandeSvc.GetAllCommandesByIdRestaurateur(commande)
        
        const message: string = 'Get all commandes by id_restaurateur'
        returnSuccess(res, commande_res, message)

    } catch (err) {
        next(err)
    }
})

commandeController.get('/delivery_driver/:id_delivery_driver(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande: Commande = new Commande({
            id_delivery_driver: req.params.id_delivery_driver
        })
        // const id_commande: string = req.params.id_commande
        commande.IsIdDeliveryDriverValid()

        const commande_res = await commandeSvc.GetAllCommandesByIdDeliveryDriver(commande)

        const message: string = 'Get all commandes by id_delivery_driver'
        returnSuccess(res, commande_res, message)

    } catch (err) {
        next(err)
    }
})

commandeController.get('/commande_state/get/:state', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande: Commande = new Commande({
            commande_state: req.params.state
        })

        commande.IsCommandeStateValid()

        const commande_res = await commandeSvc.GetAllCommandesCommandeState(commande)

        const message: string = 'Get all commandes by commande_state'
        returnSuccess(res, commande_res, message)

    } catch (err) {
        next(err)
    }
})

commandeController.get('/commande_state/restaurateur/:state', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande: Commande = new Commande({
            commande_state: req.params.state,
            id_restaurateur: parseInt(req.body.id_restaurateur)
        })

        commande.IsCommandeStateValid()
        commande.IsIdRestaurateurValid()

        const commande_res = await commandeSvc.GetAllCommandesCommandeStateByRestaurateurId(commande)

        const message: string = 'Get all commandes by commande_state'
        returnSuccess(res, commande_res, message)

    } catch (err) {
        next(err)
    }
})

commandeController.get('/commande_state/client/:state', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande: Commande = new Commande({
            commande_state: req.params.state,
            id_client: parseInt(req.body.id_client),
        })

        commande.IsCommandeStateValid()
        commande.IsIdClientValid()

        const commande_res = await commandeSvc.GetAllCommandesCommandeStateByClientId(commande)

        const message: string = 'Get all commandes by commande_state'
        returnSuccess(res, commande_res, message)

    } catch (err) {
        next(err)
    }
})

commandeController.get('/commande_state/delivery_driver/:state', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande: Commande = new Commande({
            commande_state: req.params.state,
            id_delivery_driver: parseInt(req.body.id_delivery_driver),
        })

        commande.IsCommandeStateValid()
        commande.IsIdDeliveryDriverValid()

        const commande_res = await commandeSvc.GetAllCommandesCommandeStateByDeliveryDriverId(commande)

        const message: string = 'Get all commandes by commande_state'
        returnSuccess(res, commande_res, message)

    } catch (err) {
        next(err)
    }
})

commandeController.get('/waiting', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande_res = await commandeSvc.GetAllCommandesWaiting()

        const message: string = 'Get all commandes by type waiting'
        returnSuccess(res, commande_res, message)

    } catch (err) {
        next(err)
    }
})

commandeController.post('/add', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande: Commande = new Commande({
            id_client: req.body.id_client,
            id_restaurateur: req.body.id_restaurateur,
            client_email: req.body.client_email,
            client_name: req.body.client_name,
            client_first_name: req.body.client_first_name,
            restaurant_name: req.body.restaurant_name,
            commande_state: "pending",
            price: req.body.price,
            products: ConvertHttpRequestToProduct(req.body.products),
            client_address: new Address({
                street: req.body.client_address.street,
                zip_code: req.body.client_address.zip_code,
                city: req.body.client_address.city,
            }),
            restaurant_address: new Address({
                street: req.body.restaurant_address.street,
                zip_code: req.body.restaurant_address.zip_code,
                city: req.body.restaurant_address.city,
            })
        })

        commande.IsIdClientValid()
        commande.IsIdRestaurateurValid()
        commande.IsClientEmailValid()
        commande.IsClientNameValid()
        commande.IsClientFirstNameValid()
        commande.IsRestaurantNameValid()
        commande.IsCommandeStateValid()
        commande.IsPriceValid()
        commande.client_address.IsAddressValid()
        commande.restaurant_address.IsAddressValid()

        for (let i = 0; i < commande.products.length; i++) {
            commande.products[i].IsProduceValid()
        }

        commandeSvc.InsertCommande(commande);
        
        const message: string = 'Add commande'
        returnCreated(res, message)

    } catch (err) {
        next(err)
    }
})

commandeController.put('/update/:id_commande',async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande: Commande = new Commande({
            id_commande: req.params.id_commande,
            id_delivery_driver: req.body.id_delivery_driver,
            delivery_driver_name: req.body.delivery_driver_name,
            delivery_driver_first_name: req.body.delivery_driver_first_name,
            commande_state: req.body.commande_state,
        })

        commande.IsIdCommandeValid()

        await commandeSvc.UpdateCommande(commande)

        const message: string = 'Add commande by id'
        returnUpdated(res, message)

    } catch (err) {
        next(err)
    }
})

export {commandeController}