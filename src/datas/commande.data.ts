import { CommandeModel } from '../services/mongo.service';
import { Commande } from '../models/commande.model';
import { createPrinter } from 'typescript';

export class commandeService {

    public async GetCommandeById(commande: Commande) {
        const res = await CommandeModel.findById(commande.id_commande);
        
        return res
    }

    public async GetAllCommandesByIdClient(commande: Commande) {
        const res = await CommandeModel.find({ "id_client": commande.id_client})
            .sort({ creation_date: -1 });

        return res
    }

    public async GetAllCommandesByIdRestaurateur(commande: Commande) {
        const res = await CommandeModel.find({ "id_restaurateur": commande.id_restaurateur })
            .sort({ creation_date: -1 });
            
        return res
    }

    public async GetAllCommandesByIdDeliveryDriver(commande: Commande) {
        const res = await CommandeModel.find({ "id_delivery_driver": commande.id_delivery_driver })
            .sort({ creation_date: -1 });

        return res
    }

    public async GetAllCommandesCommandeState(commande: Commande) {
        const res = await CommandeModel.find({ commande_state: commande.commande_state })
            .sort({ creation_date: -1 });

        return res
    }

    public async GetAllCommandesCommandeStateByRestaurateurId(commande: Commande) {
        const res = await CommandeModel.find({ 
            commande_state: commande.commande_state,
            id_restaurateur: commande.id_restaurateur
        }).sort({ creation_date: -1 });

        return res
    }

    public async GetAllCommandesCommandeStateByClientId(commande: Commande) {
        const res = await CommandeModel.find({
            commande_state: commande.commande_state,
            id_client: commande.id_client
        }).sort({ creation_date: -1 });

        return res
    }

    public async GetAllCommandesCommandeStateByDeliveryDriverId(commande: Commande) {
        const res = await CommandeModel.find({
            commande_state: commande.commande_state,
            id_delivery_driver: commande.id_delivery_driver
        }).sort({ creation_date: -1 });

        return res
    }

    public async GetAllCommandesWaiting() {
        const res = await CommandeModel.find({ commande_state: "waiting" })
            .sort({ creation_date: -1 });

        return res
    }

    public async InsertCommande(commande: Commande) {
        var article = new CommandeModel(commande)
        article.save()
    }

    public async UpdateCommandeState(commande: Commande) {
        await CommandeModel.findOneAndUpdate(
            { _id: commande.id_commande },
            { $push: { commande_state: commande.commande_state } },
        );
    }

    public async UpdateCommande(commande: Commande) {
        const filter = { _id: commande.id_commande };
        const update = { 
            id_delivery_driver: commande.id_delivery_driver,
            delivery_driver_name: commande.delivery_driver_name,
            delivery_driver_first_name: commande.delivery_driver_first_name,
            commande_state: commande.commande_state
        };

        await CommandeModel.findOneAndUpdate(filter, update); 
    }
}
