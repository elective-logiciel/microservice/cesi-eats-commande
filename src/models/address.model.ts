import { Error400 } from "../errors/errors";

export class Address {
    street: string
    zip_code: string
    city: string

    public constructor(init?: Partial<Address>) {
        Object.assign(this, init);
    }

    public IsAddressValid(): boolean {
        this.IsStreetValid()
        this.IsZipCodeValid()
        this.IsCityValid()

        return true
    }

    public IsStreetValid(): boolean {
        if (this.street === undefined) {
            throw new Error400("Missing argument, 'street' can not be NULL")
        }
        if (this.street === "") {
            throw new Error400("Empty argument, 'street' can not be EMPTY")
        }
        return true
    }

    public IsZipCodeValid(): boolean {
        if (this.zip_code === undefined) {
            throw new Error400("Missing argument, 'zip_code' can not be NULL")
        }
        if (this.zip_code === "") {
            throw new Error400("Empty argument, 'zip_code' can not be EMPTY")
        }
        return true
    }

    public IsCityValid(): boolean {
        if (this.city === undefined) {
            throw new Error400("Missing argument, 'city' can not be NULL")
        }
        if (this.city === "") {
            throw new Error400("Empty argument, 'city' can not be EMPTY")
        }
        return true
    }
}