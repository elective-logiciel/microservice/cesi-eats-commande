import { Error400 } from "../errors/errors"

export class Article {
    name: string
    price: number
    img: string
    description: string

    public constructor(init?: Partial<Article>) {
        Object.assign(this, init);
    }

    public IsArticleValid(): boolean {
        this.IsNameValid()
        // this.IsImgValid()
        this.IsPriceValid()
        // this.IsDescriptionValid()

        return true
    }

    public IsNameValid(): boolean {
        if (this.name === undefined) {
            throw new Error400("Missing argument, 'name' can not be NULL")
        }
        if (this.name === "") {
            throw new Error400("Empty argument, 'name' can not be EMPTY")
        }
        return true
    }

    public IsImgValid(): boolean {
        if (this.img === undefined) {
            throw new Error400("Missing argument, 'img' can not be NULL")
        }
        if (this.img === "") {
            throw new Error400("Empty argument, 'img' can not be EMPTY")
        }
        return true
    }

    public IsDescriptionValid(): boolean {
        if (this.description === undefined) {
            throw new Error400("Missing argument, 'description' can not be NULL")
        }
        if (this.description === "") {
            throw new Error400("Empty argument, 'description' can not be EMPTY")
        }
        return true
    }

    public IsPriceValid(): boolean {
        if (this.price === undefined) {
            throw new Error400("Missing argument, 'price' can not be NULL")
        }
        if (this.price < 0) {
            throw new Error400("Out of range argument, 'price' can not be a negative number")
        }
        return true
    }
}