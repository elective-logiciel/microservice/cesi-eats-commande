import { Error400 } from "../errors/errors"
import { Product } from "./product.model"
import { Address } from "./address.model"

export class Commande {
    id_commande: string
    id_client: number
    id_restaurateur: number
    id_delivery_driver: number | null
    client_email: string 
    client_name: string
    client_first_name: string
    restaurant_name: string
    delivery_driver_name: string | null
    delivery_driver_first_name: string | null
    commande_state: string
    order_time: string
    price: number
    creation_date: Date
    products: Product[]
    client_address: Address
    restaurant_address: Address

    public constructor(init?: Partial<Commande>) {
        Object.assign(this, init);
    }

    public IsIdCommandeValid(): boolean {
        if (this.id_commande === undefined) {
            throw new Error400("Missing argument, 'id_commande' can not be NULL")
        }
        if (this.id_commande === "") {
            throw new Error400("Empty argument, 'id_commande' can not be EMPTY")
        }
        return true
    }


    public IsIdClientValid(): boolean {
        if (this.id_client === undefined) {
            throw new Error400("Missing argument, 'id_client' can not be NULL")
        }
        if (this.id_client < 0) {
            throw new Error400("Out of range argument, 'id_client' can not be a negative number")
        }
        return true
    }

    public IsIdRestaurateurValid(): boolean {
        if (this.id_restaurateur === undefined) {
            throw new Error400("Missing argument, 'id_restaurateur' can not be NULL")
        }
        if (this.id_restaurateur < 0) {
            throw new Error400("Out of range argument, 'id_restaurateur' can not be a negative number")
        }
        return true
    }

    public IsIdDeliveryDriverValid(): boolean {
        if (this.id_delivery_driver === undefined) {
            throw new Error400("Missing argument, 'id_delivery_driver' can not be NULL")
        }
        // if (this.id_delivery_driver < 0) {
        //     throw new Error400("Out of range argument, 'id_delivery_driver' can not be a negative number")
        // }
        return true
    }

    public IsClientEmailValid(): boolean {
        if (this.client_email === undefined) {
            throw new Error400("Missing argument, 'client_email' can not be NULL")
        }
        if (this.client_email === "") {
            throw new Error400("Empty argument, 'client_email' can not be EMPTY")
        }
        return true
    }

    public IsClientNameValid(): boolean {
        if (this.client_name === undefined) {
            throw new Error400("Missing argument, 'client_name' can not be NULL")
        }
        if (this.client_name === "") {
            throw new Error400("Empty argument, 'client_name' can not be EMPTY")
        }
        return true
    }

    public IsClientFirstNameValid(): boolean {
        if (this.client_first_name === undefined) {
            throw new Error400("Missing argument, 'client_first_name' can not be NULL")
        }
        if (this.client_first_name === "") {
            throw new Error400("Empty argument, 'client_first_name' can not be EMPTY")
        }
        return true
    }

    public IsRestaurantNameValid(): boolean {
        if (this.restaurant_name === undefined) {
            throw new Error400("Missing argument, 'restaurant_name' can not be NULL")
        }
        if (this.restaurant_name === "") {
            throw new Error400("Empty argument, 'restaurant_name' can not be EMPTY")
        }
        return true
    }

    public IsDeliveryDriverNameValid(): boolean {
        if (this.delivery_driver_name === undefined) {
            throw new Error400("Missing argument, 'delivery_driver_name' can not be NULL")
        }
        if (this.delivery_driver_name === "") {
            throw new Error400("Empty argument, 'delivery_driver_name' can not be EMPTY")
        }
        return true
    }

    public IsDeliveryDriverFirstNameValid(): boolean {
        if (this.delivery_driver_first_name === undefined) {
            throw new Error400("Missing argument, 'delivery_driver_first_name' can not be NULL")
        }
        if (this.delivery_driver_first_name === "") {
            throw new Error400("Empty argument, 'delivery_driver_first_name' can not be EMPTY")
        }
        return true
    }

    public IsCommandeStateValid(): boolean {
        if (this.commande_state === undefined) {
            throw new Error400("Missing argument, 'commande_state' can not be NULL")
        }
        if (this.commande_state !== "pending" && this.commande_state !== "refused" && this.commande_state !== "waiting" && this.commande_state !== "preparing" && this.commande_state !== "delivering" && this.commande_state !== "delivered") {
            throw new Error400("Wrong argument, 'commande_state' must be egal to 'pending', 'refused', 'waiting', 'preparing', 'delivering' or 'delivered'")
        }
        return true
    }

    public IsPriceValid(): boolean {
        if (this.price === undefined) {
            throw new Error400("Missing argument, 'price' can not be NULL")
        }
        if (this.price < 0) {
            throw new Error400("Out of range argument, 'price' can not be a negative number")
        }
        return true
    }
}