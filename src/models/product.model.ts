import { Error400 } from "../errors/errors"

import { Article } from "./article.model"

export class Product {
    name: string
    type_menu: boolean
    price: number
    img: string
    description: string
    articles: Article[]

    public constructor(init?: Partial<Product>) {
        Object.assign(this, init);
    }

    public IsProduceValid(): boolean {
        this.IsNameValid()
        // this.IsImgValid()
        this.IsPriceValid()
        // this.IsDescriptionValid()

        if (this.articles !== undefined) {
            for (let i = 0; i < this.articles.length; i++) {
                this.articles[i].IsArticleValid()
            }
        }

        return true
    }

    public IsNameValid(): boolean {
        if (this.name === undefined) {
            throw new Error400("Missing argument, 'name' can not be NULL")
        }
        if (this.name === "") {
            throw new Error400("Empty argument, 'name' can not be EMPTY")
        }
        return true
    }

    public IsImgValid(): boolean {
        if (this.img === undefined) {
            throw new Error400("Missing argument, 'img' can not be NULL")
        }
        if (this.img === "") {
            throw new Error400("Empty argument, 'img' can not be EMPTY")
        }
        return true
    }

    public IsDescriptionValid(): boolean {
        if (this.description === undefined) {
            throw new Error400("Missing argument, 'description' can not be NULL")
        }
        if (this.description === "") {
            throw new Error400("Empty argument, 'description' can not be EMPTY")
        }
        return true
    }

    public IsPriceValid(): boolean {
        if (this.price === undefined) {
            throw new Error400("Missing argument, 'price' can not be NULL")
        }
        if (this.price < 0) {
            throw new Error400("Out of range argument, 'price' can not be a negative number")
        }
        return true
    }
}

export function ConvertHttpRequestToProduct(req_product): Product[] {
    var product_list: Product[] = []

    for (let i = 0; i < req_product.length; i++) {
        let product = req_product[i]

        if (product.type_menu) {
            var new_product: Product = new Product({
                name: product.name,
                type_menu: product.type_menu,
                price: product.price,
                img: product.img,
                description: product.description,
                articles: []
            })

            for (let j = 0; j < product.articles.length; j++) {
                var article = product.articles[j]

                new_product.articles.push(new Article({
                    name: article.name,
                    price: article.price,
                    img: article.img,
                    description: article.description,
                }))
            }

            product_list.push(new_product)
        } else {
            product_list.push(new Product({
                name: product.name,
                type_menu: product.type_menu,
                price: product.price,
                img: product.img,
                description: product.description
            }))
        }
    }

    return product_list
}