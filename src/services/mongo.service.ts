import { connect, Schema, model } from 'mongoose';
import { Commande } from '../models/commande.model';
import { Product } from '../models/product.model';
import { Article } from '../models/article.model';
import { Address } from '../models/address.model';

const config = require('../config')

const mongoDb = connect(config.mongo.server + config.mongo.database);

const ArticleSchema = new Schema<Article>({
    name: { type: String, required: true },
    price: { type: Number, required: true },
    img: { type: String, required: false },
    description: { type: String, required: false }
})

const ProductSchema = new Schema<Product>({
    name: { type: String, required: true },
    type_menu: { type: Boolean, required: true },
    price: { type: Number, required: true },
    img: { type: String, required: false },
    description: { type: String, required: false },
    articles: { type: [ArticleSchema], required: false }
})

const AddressSchema = new Schema<Address>({
    street: { type: String, required: true },
    zip_code: { type: String, required: true },
    city: { type: String, required: true },
})

const CommandeSchema = new Schema<Commande>({
    id_client: { type: Number, required: true },
    id_restaurateur: { type: Number, required: true },
    id_delivery_driver: { type: Number, required: false },
    client_email: { type: String, required: true },
    client_name: { type: String, required: true },
    client_first_name: { type: String, required: true },
    restaurant_name: { type: String, required: true },
    delivery_driver_name: { type: String, required: false },
    delivery_driver_first_name: { type: String, required: false },
    commande_state: { type: String, required: true },
    price: { type: Number, required: true },
    creation_date: { type: Date, default: Date.now },
    products: { type: [ProductSchema], required: true },
    client_address: { type: AddressSchema, require: true },
    restaurant_address: { type: AddressSchema, required: true },
})

export const CommandeModel = model<Commande>('Commande', CommandeSchema)


// export class Mongo {

//     public connect() {
//         const mongoDb = connect(config.mongo.server + config.mongo.database);
//     }

//     public close() {

//     }
// }